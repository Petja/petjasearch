<?php
	class Search {
		public $query;
		
		public function __construct($query){
			if($query == NULL){
				$query = "";
			}
			$this->query = $query;
		}
		 
		/**
		 * Muuntaa kyselystringin hakukyselyobjektiksi
		 */
		public function query2object(){
			$this->query = str_replace("=", ":", $this->query);
			$ret = array("search" => "", "params" => array());
		
			try{
				$rex = '/(?:\s|)(\w)+:((\w|\d|\.|,)+|"(\w|\d|\s|\.|,)+")/';
				preg_match_all($rex,$this->query,$mat);
				
				var_dump("RESULT",$mat);
					
				$mat = $mat[0];
					
				foreach($mat as $v){
					$parts = explode(":",$v);
					$ret["params"][trim($parts[0],$this->trimchars)] = trim($parts[1],$this->trimchars);
				}
					
				$ret["search"] = trim(preg_replace($rex, "", $this->query),$this->trimchars);
			}catch(Exception $e){
				$ret["search"] = $this->query;
			}
			
			//var_dump($ret);
		
			return $ret;
		}
		
		/**
		 * Muuntaa hakukyselyobjektin kyselystringiksi
		 * @param Hakukysely $obj
		 * @return string
		 */
		public function object2query($obj){
			$ret = array($obj["search"]);
			foreach($obj["params"] as $k => $v){
				$ret[] = $k . ":" . $v;
			}
			return trim(implode(json_decode("\"\u2003\""),$ret),$this->trimchars);
		}
		
		public function getMultiParam($key){
			$tmp = $this->getParam($key,$this->query);
			if($tmp != ""){
				return explode(",",$tmp);
			}else{
				return array();
			}
		}
		
		/**
		 * Asettaa parametriin arrayn
		 * @param string $key
		 * @param array $arr
		 * @return string
		 */
		public function setMultiParam($key,$arr){
			return $this->setParam($key,implode(",",$arr),$this->query);
		}
		
		/**
		 * Asettaa uuden alkion array-muotoiseen hakukyselyn parametriin
		 * @param string $key
		 * @param string $value
		 * @return string
		 */
		public function addMultiParam($key,$value){
			$arr =  $this->getMultiParam($key,$this->query);
			$arr[] = $value;
			$this->query = $this->setMultiParam($key,$arr,$this->query);
			return $this->getQuery();
		}
		
		/**
		 * Poistaa arrayn alkion hakykyselyn parametrista
		 * @param string $key
		 * @param string $value
		 * @return string
		 */
		public function removeMultiParam($key,$value){
			$arr = $this->getMultiParam($key,$this->query);
			$pos = array_search($value,$arr);
			if($pos !== false){
				array_splice($arr, $pos, 1);
			}
			$this->query = $this->setMultiParam($key,$arr,$this->query);
			return $this->getQuery();
		}
		
		/**
		 * Asettaa parametrin hakukyselyyn
		 * @param string $key
		 * @param string $value
		 * @return string
		 */
		public function setParam($key,$value){
			$tmp = $this->query2object($this->query);
			unset($tmp["params"][$key]);
			if($value !== false && $value !== ""){
				$tmp["params"][$key] = $value;
			}
			$this->query = $this->object2query($tmp);
			return $this;
		}
		
		/**
		 * Asettaa päivämäärämuotoisen parametrin hakukyselyyn
		 * @param string $key
		 * @param string $value
		 * @return string
		 */
		public function setDateParam($key,$value){
			return $this->setParam($key,$value->format("j.n.Y"));
		}
		
		/**
		 * Palauttaa pyydetyn parametrin hakulausekkeesta
		 * @param string $key
		 */
		public function getParam($key){
			$tmp = $this->query2object($this->query);
			return $tmp["params"][$key];
		}
		
		/**
		 * Palauttaa pyydetyn parametrin hakulausekkeesta päivämäärämuodossa
		 * @param string $key
		 */
		public function getDateParam($key){
			if($tmp = $this->getParam($key) != null){
				$ret = new DateTime;
				$ret->setTimestamp(strtotime($tmp));
			}else{
				$ret = null;
			}
			return $ret;
		}
		
		/**
		 * Palauttaa hakukyselyn hakusanan
		 */
		public function getSearch(){
			$tmp = $this->query2object($this->query);
			return $tmp["search"];
		}
		
		/**
		 * Asettaa hakukyselyyn uuden hakusanan
		 */
		public function setSearch($search){
			$tmp = $this->query2object($this->query);
			$tmp["search"] = $search;
			return $tmp;
		}
		
		/**
		 * Luo uudelleen hakukyselyn (Onko tarpeellinen?)
		 */
		public function reformatQuery(){
			return $this->object2query($this->query2object($this->query));
		}
		
		public function getQuery(){
			return $this->query;
		}
	}
?>
